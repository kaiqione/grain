export enum PermissionsEnum {
  DATA_ASSETS_MENU = 'data_assets_menu', // 数据资产
  /**
   * 资产管理
   */
  ASSETS_MENU = 'assets_manage_menu', // 资产管理
  ASSETS_ADD = 'assets_manage_add', // 资产添加
  ASSETS_DEL = 'assets_manage_del', // 资产删除
  ASSETS_EDIT = 'assets_manage_edit', // 资产修改
  ASSETS_GROUP_ADD = 'assets_group_add', // 资产组添加
  ASSETS_GROUP_DEL = 'assets_group_del', // 资产组删除
  ASSETS_GROUP_EDIT = 'assets_group_edit', // 资产组编辑
  /**
   * 元数据管理
   */
  METAS_MENU = 'metas_menu', // 菜单
  METAS_PREVIEW = 'metas_preview', // 预览部分数据
  METAS_UPDATE = 'metas_update', // 元数据更新字段

  /**
   * 系统管理
   */
  SYSTEM_MANAGE = 'system_manage_menu',

  PERMISSION_MENU = 'permission_menu', // 权限管理
  ROLE_MENU = 'role_menu', // 角色管理
  USER_GROUP_MENU = 'user_group_menu', // 用户组管理
  ACCOUNT_MENU = 'account_menu', // 用户管理
  AUDIT_MENU = 'audit_menu', // 审计管理菜单

  ACCOUNT_PERMISSION_ADD = 'account_permission_add', // 权限添加
  ACCOUNT_PERMISSION_DEL = 'account_permission_del', // 权限删除
  ACCOUNT_PERMISSION_UPDATE = 'account_permission_edit', // 权限编辑

  ACCOUNT_ROLE_ADD = 'account_role_add', // 角色添加
  ACCOUNT_ROLE_DEL = 'account_role_del', // 角色删除
  ACCOUNT_ROLE_UPDATE = 'account_role_edit', // 角色编辑

  ACCOUNT_GROUP_ADD = 'account_group_add', // 用户组添加
  ACCOUNT_GROUP_DEL = 'account_group_del', // 用户组删除
  ACCOUNT_GROUP_UPDATE = 'account_group_edit', // 用户组编辑

  ACCOUNT_USER_ADD = 'account_user_add', // 用户添加
  ACCOUNT_USER_DEL = 'account_user_del', // 用户删除
  ACCOUNT_USER_UPDATE = 'account_user_edit', // 用户编辑

  /**
   * 仪表板
   */
  PANEL_MENU = 'panel_menu', // 仪表板菜单

  PANEL_ADD = 'panel_add',
  PANEL_UPDATE = 'panel_update',
  PANEL_DEL = 'panel_del',
  PANEL_SHARE = 'panel_share',
}

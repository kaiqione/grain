/*
 * @*      ____       _             :  ******************************
 * @*     / __ )___  (_)___  __  __ :  Author:Beipy
 * @*    / __  / _ \/ / __ \/ / / / :  WebURL:http://www.beipy.com
 * @*   / /_/ /  __/ / /_/ / /_/ /  :  Github:http://github.com/beipy
 * @*  /_____/\___/_/ .___/\__, /   :  别有梓人传，精艺夺天工。便使玉人雕琢，妙手略相同。
 * @*              /_/    /____/    :  ******************************
 * @Descripttion:
 * @Date: 2022-04-06 17:44:50
 * @LastEditors: Beipy
 * @FilePath: /govern_web/src/enums/engine.ts
 * @LastEditTime: 2022-04-29 10:12:43
 */

/**
 * @description: 封装统一引擎类型
 */
export const engineEnum = (): any[] => [
  { type: '001', title: '数据清洗', list: [] },
  { type: '002', title: '数据过滤', list: [] },
  { type: '003', title: '数据转换', list: [] },
  { type: '004', title: '数据脱敏', list: [] },
  { type: '005', title: '数据合并', list: [] },
  { type: '006', title: '数据打标签', list: [] },
  { type: '008', title: '数据生命周期', list: [] },
  { type: '009', title: '数据质量管理', list: [] },
  { type: '011', title: '文档数据转换', list: [] },
  { type: '012', title: '半结构化数据转换', list: [] },
  { type: '013', title: '二进制流转换', list: [] },
  { type: '014', title: '地理位置', list: [] },
  //   { type: '016', title: 'HTTP', list: [] },
  { type: '017', title: '文本过滤', list: [] },
  { type: '100', title: '视频处理', list: [] },
];

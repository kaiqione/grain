// 表格属性
export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    width: 50,
    fixed: 'left',
    slots: { customRender: 'index' },
  },
  {
    title: '名称',
    dataIndex: 'name',
    fixed: 'left',
    width: 100,
    ellipsis: true,
    slots: { customRender: 'name' },
  },
  {
    title: '单价（元/斤）',
    dataIndex: 'price',
    width: 100,
    slots: { customRender: 'price' },
  },
  {
    title: '备注',
    dataIndex: 'doc',
    width: 300,
    slots: { customRender: 'doc' },
  },
  {
    title: '创建时间',
    dataIndex: 'insert_time',
    width: 150,
    slots: { customRender: 'insert_time' },
  },
  {
    title: '修改时间',
    dataIndex: 'update_time',
    width: 150,
    slots: { customRender: 'update_time' },
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 150,
    fixed: 'right',
    slots: { customRender: 'action' },
  },
];

export const SearchItems = [
  {
    name: 'name',
    type: 'input',
    placeholder: '请输粮食名称',
  },
];

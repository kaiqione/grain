import { getPersons } from '../../person/common/mock';

export const delGrain = (encode: any) => {
  const grains: any[] = JSON.parse(localStorage.getItem('grains') ?? '[]');
  const index: number = grains.findIndex((ele) => {
    return ele.encode == encode;
  });
  if (index != -1) {
    grains.splice(index, 1);
  }
  localStorage.setItem('grains', JSON.stringify(grains));
};

export const addGrains = (grain: any) => {
  const grains: any[] = JSON.parse(localStorage.getItem('grains') ?? '[]');
  grains.push(grain);
  localStorage.setItem('grains', JSON.stringify(grains));
};

export const getGrains = (params: any) => {
  const grains: any[] = JSON.parse(localStorage.getItem('grains') ?? '[]');
  if (params.pageSize == 0) {
    return { total: grains.length, items: grains };
  }
  return {
    total: grains.length,
    items: grains.slice((params.page - 1) * params.pageSize, params.pageSize + 1),
  };
};

export const getPersonName = (pId: any) => {
  const target: any = getPersons({
    page: 1,
    pageSize: 0,
  }).items.find((ele) => {
    return ele.id == pId;
  });

  return target ? target.name : pId;
};

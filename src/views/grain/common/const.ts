// 表格属性
export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    width: 50,
    fixed: 'left',
    slots: { customRender: 'index' },
  },
  {
    title: '单号',
    dataIndex: 'encode',
    fixed: 'left',
    width: 150,
    ellipsis: true,
    slots: { customRender: 'encode' },
  },
  {
    title: '粮食种类',
    dataIndex: 'type',
    width: 150,
    slots: { customRender: 'type' },
  },
  {
    title: '单价（元/斤）',
    dataIndex: 'price',
    width: 120,
    slots: { customRender: 'price' },
  },
  {
    title: '总重（吨）',
    dataIndex: 'weight_t',
    width: 100,
    slots: { customRender: 'weight_t' },
  },
  {
    title: '皮重（吨）',
    dataIndex: 'weight_p',
    width: 100,
    slots: { customRender: 'weight_p' },
  },
  {
    title: '净重（吨）',
    dataIndex: 'weight',
    width: 100,
    slots: { customRender: 'weight' },
  },
  {
    title: '金额（元）',
    dataIndex: 'sum',
    width: 100,
    slots: { customRender: 'sum' },
  },
  {
    title: '入库/出库',
    dataIndex: 'storage_type',
    width: 100,
    slots: { customRender: 'storage_type' },
  },
  {
    title: '入库/出库时间',
    dataIndex: 'insert_time',
    width: 150,
    slots: { customRender: 'insert_time' },
  },
  {
    title: '修改时间',
    dataIndex: 'update_time',
    width: 150,
    slots: { customRender: 'update_time' },
  },
  {
    title: '买主姓名',
    dataIndex: 'buyer',
    width: 100,
    slots: { customRender: 'buyer' },
  },

  {
    title: '卖主姓名',
    dataIndex: 'seller',
    width: 100,
    slots: { customRender: 'seller' },
  },
  {
    title: '联系方式',
    dataIndex: 'phone',
    width: 100,
    slots: { customRender: 'phone' },
  },
  {
    title: '载车牌号',
    dataIndex: 'car_id',
    width: 100,
    slots: { customRender: 'car_id' },
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 100,
    slots: { customRender: 'status' },
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 150,
    fixed: 'right',
    slots: { customRender: 'action' },
  },
];

/**
 * 粮食种类
 */
export const GrainTypeOptions = [
  {
    label: '玉米',
    value: 'corn',
  },
];

/**
 * 粮食等级
 */
export const GrainLevelOptions = [
  {
    label: 'Ⅰ',
    value: 'Ⅰ',
  },
  {
    label: 'Ⅱ',
    value: 'Ⅱ',
  },
  {
    label: 'Ⅲ',
    value: 'Ⅲ',
  },
  // {
  //   label: 'Ⅳ',
  //   value: 'Ⅳ',
  // },
  // {
  //   label: 'Ⅴ',
  //   value: 'Ⅴ',
  // },
  // {
  //   label: 'Ⅵ',
  //   value: 'Ⅵ',
  // },
];

/**
 * 重量单位
 */
export const GrainWeightUOptions = [
  {
    label: '千克',
    value: 'kg',
  },
  {
    label: '吨',
    value: 'ton',
  },
];

/**
 * 仓库
 */
export const GrainWarehouseOptions = [
  {
    label: '一号仓库',
    value: '1',
  },
  {
    label: '二号仓库',
    value: '2',
  },
  {
    label: '三号仓库',
    value: '3',
  },
];

/**
 * 出入库类型
 */
export const Grainstorage_typeOptions = [
  {
    label: '出库',
    value: 'out',
  },
  {
    label: '入库',
    value: 'in',
  },
];

export const SearchItems = [
  {
    name: 'insert_time',
    type: 'datepicker',
    placeholder: '请选择时间',
  },
  {
    name: 'type',
    type: 'select',
    placeholder: '请选择粮食类型',
    options: GrainTypeOptions,
  },

  {
    name: 'storage_type',
    type: 'select',
    placeholder: '请选择出入库类型',
    options: Grainstorage_typeOptions,
  },
  {
    name: 'buyer',
    type: 'select',
    placeholder: '请选择买家',
    options: [],
  },
  {
    name: 'seller',
    type: 'select',
    placeholder: '请选择卖家',
    options: [],
  },
  {
    name: 'status',
    type: 'select',
    placeholder: '请选择状态',
    options: [
      {
        label: '未完成',
        value: 0,
      },
      {
        label: '完成',
        value: 1,
      },
    ],
  },
];

export const statList = [
  {
    color: '#427df6',
    label: '当前库存总量',
    key: 'on_total',
    tag: 'all',
  },
  {
    color: '#506874',
    label: '历史入库总量',
    key: 'in_total',
    tag: '入',
  },
  {
    color: '#506874',
    label: '历史出库总量',
    key: 'out_total',
  },
  {
    color: '#327335',
    label: '今日入库总量',
    key: 'today_in_total',
  },
  {
    color: '#327335',
    label: '今日出库总量',
    key: 'today_out_total',
  },
  {
    color: '#945800',
    label: '昨日入库总量',
    key: 'yesterday_in_total',
  },
  {
    color: '#945800',
    label: '昨日出库总量',
    key: 'yesterday_out_total',
  },
];

/**
 * 获取粮食类型
 */
export const getGrainType = (type: string) => {
  const traget: any = GrainTypeOptions.find((ele) => {
    return ele.value == type;
  });

  return traget?.label ?? type;
};

/**
 * 获取仓库名
 * @param value
 * @returns
 */
export const getWarehouseLabel = (value: any) => {
  const target: any = GrainWarehouseOptions.find((ele) => {
    return ele.value == value;
  });

  return target ? target.label : value;
};

/**
 * 数字转大写
 * @param money
 * @returns
 */

export const convertCurrency = (money) => {
  // 汉字的数字
  const cnNums = ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'];
  // 基本单位
  const cnIntRadice = ['', '拾', '佰', '仟'];
  // 对应整数部分扩展单位
  const cnIntUnits = ['', '万', '亿', '兆'];
  // 对应小数部分单位
  const cnDecUnits = ['角', '分', '毫', '厘'];
  // 整数金额时后面跟的字符
  const cnInteger = '整';
  // 整型完以后的单位
  const cnIntLast = '元';
  if (money == '') {
    return '';
  }
  money = parseFloat(money);
  if (isNaN(money) || money >= 1000000000000) {
    return '';
  }
  money = money.toFixed(2); // 转换为字符串，并保留两位小数

  let chineseStr = ''; // 输出的中文金额字符串
  const parts: string[] = money.split('.'); // 分离金额后用的数组，预定义
  const integerNum = parts[0];
  const decimalNum = parts[1];
  if (parseInt(integerNum) > 0) {
    let zeroCount = 0;
    const IntLen = integerNum.length;
    for (let i = 0; i < IntLen; i++) {
      const n = integerNum.substr(i, 1);
      const p = IntLen - i - 1;
      const q = p / 4;
      const m = p % 4;
      if (n == '0') {
        zeroCount++;
      } else {
        if (zeroCount > 0) {
          chineseStr += cnNums[0];
        }
        zeroCount = 0;
        chineseStr += cnNums[parseInt(n)] + cnIntRadice[m];
      }
      if (m == 0 && zeroCount < 4) {
        chineseStr += cnIntUnits[q];
      }
    }
    chineseStr += cnIntLast;
  }
  if (decimalNum != '') {
    const decLen = decimalNum.length;
    for (let i = 0; i < decLen; i++) {
      const n = decimalNum.substr(i, 1);
      if (n != '0') {
        chineseStr += cnNums[Number(n)] + cnDecUnits[i];
      }
    }
  }
  if (chineseStr == '') {
    chineseStr += cnNums[0] + cnIntLast + cnInteger;
  } else if (decimalNum == '') {
    chineseStr += cnInteger;
  }
  return chineseStr;
};

export const weightTowFloat = (value: number) => {
  return parseFloat((value / 1000 / 1000).toFixed(2));
};

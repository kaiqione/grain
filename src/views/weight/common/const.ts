// 表格属性
export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    width: 50,
    fixed: 'left',
    slots: { customRender: 'index' },
  },
  {
    title: '单号',
    dataIndex: 'encode',
    fixed: 'left',
    width: 150,
    ellipsis: true,
    slots: { customRender: 'encode' },
  },
  {
    title: '总重（吨）',
    dataIndex: 'weight_t',
    width: 100,
    slots: { customRender: 'weight_t' },
  },
  {
    title: '皮重（吨）',
    dataIndex: 'weight_p',
    width: 100,
    slots: { customRender: 'weight_p' },
  },
  {
    title: '净重（吨）',
    dataIndex: 'weight',
    width: 100,
    slots: { customRender: 'weight' },
  },
  {
    title: '时间',
    dataIndex: 'insert_time',
    width: 150,
    slots: { customRender: 'insert_time' },
  },
  {
    title: '人员姓名',
    dataIndex: 'person_id',
    width: 100,
    slots: { customRender: 'person_id' },
  },
  {
    title: '联系方式',
    dataIndex: 'phone',
    width: 100,
    slots: { customRender: 'phone' },
  },
  {
    title: '载车牌号',
    dataIndex: 'car_id',
    width: 100,
    slots: { customRender: 'car_id' },
  },
  {
    title: '状态',
    dataIndex: 'status',
    width: 100,
    slots: { customRender: 'status' },
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 150,
    fixed: 'right',
    slots: { customRender: 'action' },
  },
];

/**
 * 重量单位
 */
export const GrainWeightUOptions = [
  {
    label: '千克',
    value: 'kg',
  },
  {
    label: '吨',
    value: 'ton',
  },
];

export const SearchItems = [
  {
    name: 'person_id',
    type: 'select',
    placeholder: '请选择',
    options: [],
  },

  {
    name: 'status',
    type: 'select',
    placeholder: '请选择状态',
    options: [
      {
        label: '未完成',
        value: 0,
      },
      {
        label: '完成',
        value: 1,
      },
    ],
  },
];

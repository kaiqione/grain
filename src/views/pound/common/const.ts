// 表格属性
export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    width: 50,
    fixed: 'left',
    slots: { customRender: 'index' },
  },
  {
    title: '重量（吨）',
    dataIndex: 'weight',
    fixed: 'left',
    width: 100,
    ellipsis: true,
    slots: { customRender: 'name' },
  },
  {
    title: '过磅时间',
    dataIndex: 'insert_time',
    width: 150,
    slots: { customRender: 'insert_time' },
  },
  {
    title: '空车/载货',
    dataIndex: 'type',
    width: 100,
    slots: { customRender: 'type' },
  },
  {
    title: '人员',
    dataIndex: 'person_id',
    width: 150,
    slots: { customRender: 'person_id' },
  },
  {
    title: '联系方式',
    dataIndex: 'phone',
    width: 150,
    slots: { customRender: 'phone' },
  },
  {
    title: '车牌号',
    dataIndex: 'car_id',
    width: 150,
    slots: { customRender: 'car_id' },
  },
  {
    title: '备注',
    dataIndex: 'doc',
    width: 300,
    slots: { customRender: 'doc' },
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 150,
    fixed: 'right',
    slots: { customRender: 'action' },
  },
];

export const SearchItems = [
  {
    name: 'insert_time',
    type: 'datepicker',
    placeholder: '请选择时间',
  },
];

export enum PoundCarTypeEnum {
  KONG = 'kong',
  UNKONG = 'unkong',
}

export const PoundCarTypeOptions = [
  {
    label: '空车',
    value: PoundCarTypeEnum.KONG,
  },
  {
    label: '载货',
    value: PoundCarTypeEnum.UNKONG,
  },
];

// 表格属性
export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    width: 50,
    fixed: 'left',
    slots: { customRender: 'index' },
  },
  {
    title: '车牌号',
    dataIndex: 'car_number',
    fixed: 'left',
    width: 100,
    ellipsis: true,
    slots: { customRender: 'car_number' },
  },
  {
    title: '车主',
    dataIndex: 'owners',
    width: 300,
    slots: { customRender: 'owners' },
  },
  {
    title: '备注',
    dataIndex: 'doc',
    width: 300,
    slots: { customRender: 'doc' },
  },
  {
    title: '创建时间',
    dataIndex: 'insert_time',
    width: 150,
    slots: { customRender: 'insert_time' },
  },
  {
    title: '修改时间',
    dataIndex: 'update_time',
    width: 150,
    slots: { customRender: 'update_time' },
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 150,
    fixed: 'right',
    slots: { customRender: 'action' },
  },
];

export const SearchItems = [
  {
    name: 'car_number',
    type: 'input',
    placeholder: '请输入车牌号',
  },
  {
    name: 'person',
    type: 'select',
    placeholder: '请选择人员',
  },
];

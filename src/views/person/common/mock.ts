export const Persons = [
  {
    id: 1,
    name: '张三',
    phone: '1873456123',
    carNumber: 'HD1231',
    type: 'seller',
    createTime: '2023-08-01 15:04:27 08:00',
    updateTime: '2023-08-01 15:04:27 08:00',
  },
  {
    id: 2,
    name: '李四',
    phone: '1762314321',
    carNumber: 'JD1231',
    type: 'buyer',
    createTime: '2023-08-01 15:04:27 08:00',
    updateTime: '2023-08-01 15:04:27 08:00',
  },
  {
    id: 3,
    name: '王五',
    phone: '1775672542',
    carNumber: 'JD1231',
    type: 'seller',
    createTime: '2023-08-01 15:04:27 08:00',
    updateTime: '2023-08-01 15:04:27 08:00',
  },
  {
    id: 4,
    name: '赵六',
    phone: '1558726323',
    carNumber: 'JK2316',
    type: 'buyer',
    createTime: '2023-08-01 15:04:27 08:00',
    updateTime: '2023-08-01 15:04:27 08:00',
  },
];
const tmpPers = localStorage.getItem('persons');
if (!tmpPers) {
  localStorage.setItem('persons', JSON.stringify(Persons));
}

export const delPerson = (pId: any) => {
  const persons: any[] = JSON.parse(localStorage.getItem('persons') ?? '[]');
  const index: number = persons.findIndex((ele) => {
    return ele.id == pId;
  });
  if (index != -1) {
    persons.splice(index, 1);
  }
  localStorage.setItem('persons', JSON.stringify(persons));
};

export const addPersons = (person: any) => {
  const persons: any[] = JSON.parse(localStorage.getItem('persons') ?? '[]');
  persons.push({
    id: persons.length * 2 + 1,
    ...person,
  });
  localStorage.setItem('persons', JSON.stringify(persons));
};

export const getPersons = (params: any) => {
  const persons: any[] = JSON.parse(localStorage.getItem('persons') ?? '[]');
  if (params.pageSize == 0) {
    return { total: persons.length, items: persons };
  }
  return {
    total: persons.length,
    items: persons.slice((params.page - 1) * params.pageSize, params.pageSize + 1),
  };
};

// 表格属性
export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    width: 50,
    fixed: 'left',
    slots: { customRender: 'index' },
  },
  {
    title: '姓名',
    dataIndex: 'name',
    fixed: 'left',
    width: 100,
    ellipsis: true,
    slots: { customRender: 'name' },
  },
  {
    title: '联系方式',
    dataIndex: 'phone',
    width: 150,
    slots: { customRender: 'phone' },
  },
  {
    title: '公司名称',
    dataIndex: 'company',
    width: 150,
    slots: { customRender: 'company' },
  },
  {
    title: '创建时间',
    dataIndex: 'insert_time',
    width: 150,
    slots: { customRender: 'insert_time' },
  },
  {
    title: '修改时间',
    dataIndex: 'update_time',
    width: 150,
    slots: { customRender: 'update_time' },
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 100,
    fixed: 'right',
    slots: { customRender: 'action' },
  },
];

/**
 * 粮食种类
 */
export const PersonTypeOptions = [
  {
    label: '买家',
    value: 'buyer',
  },
  {
    label: '卖家',
    value: 'seller',
  },
];

export const SearchItems = [
  {
    name: 'name',
    type: 'input',
    placeholder: '请输入姓名',
  },
  {
    name: 'company',
    type: 'input',
    placeholder: '请输入公司名称',
  },
];

// 表格属性
export const columns = [
  {
    title: '序号',
    dataIndex: 'index',
    width: 50,
    fixed: 'left',
    slots: { customRender: 'index' },
  },
  {
    title: '邮箱',
    dataIndex: 'email',
    fixed: 'left',
    width: 100,
    ellipsis: true,
    slots: { customRender: 'email' },
  },
  {
    title: '状态',
    dataIndex: 'is_active',
    width: 150,
    slots: { customRender: 'is_active' },
  },
  {
    title: '超管',
    dataIndex: 'is_superuser',
    width: 100,
    slots: { customRender: 'is_superuser' },
  },
  {
    title: '操作',
    dataIndex: 'action',
    width: 150,
    fixed: 'right',
    slots: { customRender: 'action' },
  },
];

export const SearchItems = [
  {
    name: 'name',
    type: 'input',
    placeholder: '请输入账号',
  },
];

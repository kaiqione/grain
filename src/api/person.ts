import { defHttp } from '/@/utils/http/axios';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  person = '/api/v1/person',
}

/**
 * @description: 查询人员信息
 */
export function getPersons(params: any = null, mode: ErrorMessageMode = 'message') {
  return defHttp.get(
    {
      url: Api.person,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 添加人员信息
 */
export function addPerson(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.post(
    {
      url: Api.person,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 更新人员信息
 */
export function updatePerson(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.patch(
    {
      url: Api.person + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 删除人员信息
 */
export function delPerson(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.delete(
    {
      url: Api.person + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

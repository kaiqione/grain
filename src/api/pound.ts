import { defHttp } from '/@/utils/http/axios';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  pound = '/api/v1/bang_info',
}

/**
 * @description: 查询过磅信息
 */
export function getPound(params: any = null, mode: ErrorMessageMode = 'message') {
  return defHttp.get(
    {
      url: Api.pound,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 添加过磅信息
 */
export function addPound(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.post(
    {
      url: Api.pound,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 更新过磅信息
 */
export function updatePound(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.patch(
    {
      url: Api.pound + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 删除过磅信息
 */
export function delPound(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.delete(
    {
      url: Api.pound + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

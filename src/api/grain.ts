import { defHttp } from '/@/utils/http/axios';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  grain = '/api/v1/grain',
  grainStat = '/api/v1/stats/total',
  grainStat2 = '/api/v1/grain/sum/all',
}

/**
 * @description: 查询粮食清单
 */
export function getGrains(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.get(
    {
      url: Api.grain,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 添加粮食清单
 */
export function addGrain(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.post(
    {
      url: Api.grain,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 更新粮食清单
 */
export function updateGrain(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.patch(
    {
      url: Api.grain + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 删除粮食清单
 */
export function delGrain(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.delete(
    {
      url: Api.grain + '/' + params.id,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取统计信息
 */
export function getGrainStat(params: any = null, mode: ErrorMessageMode = 'message') {
  return defHttp.get(
    {
      url: `${Api.grainStat}${params ? '/' + params.tag : ''}`,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 获取统计信息2
 */
export function getGrainStat2(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.get(
    {
      url: Api.grainStat2,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * 获取指定长度文本
 * @param text 原文本
 * @param maxL 最大长度
 * @returns
 */
export const limitTextLength = (text: string, maxL = 5) => {
  let strL = 0;
  let finalStr = '';
  for (let i = 0; i < text.length; i++) {
    if (text.charCodeAt(i) > 255) {
      strL += 2;
    } else {
      strL++;
    }
    if (strL <= maxL) {
      finalStr += text[i];
    } else {
      finalStr += '...';
      break;
    }
  }
  return finalStr;
};

/**
 * 获取文本字节长度
 * @param text 文本
 * @returns
 */
export const getTextLength = (text: string) => {
  let strL = 0;
  if (!text || text == '') {
    return strL;
  }
  for (let i = 0; i < text.length; i++) {
    if (text.charCodeAt(i) > 255) {
      strL += 2;
    } else {
      strL++;
    }
  }
  return strL;
};

export const DataTypeList = ['string', 'number', 'boolean', 'object'];

/**
 * 数据源类型
 */
export enum DsTypeEnum {
  MYSQL = 1,
  DAMENG = 2,
  ORACALE = 3,
  PGSQL = 4,
  GOS = 5,
  REDIS = 6,
  HDFS = 7,
  HIVE = 8,
  MONGODB = 9,
  FTP = 10,
  PULSAR = 11,
  VIDEOSTREAM = 12,
  FASTDFS = 13,
  HBASE = 14,
  ES = 16,
}

export const getDsNameByType = (type: number) => {
  switch (type) {
    case DsTypeEnum.MYSQL:
      return 'MYSQL';
    case DsTypeEnum.DAMENG:
      return 'DAMENG';
    case DsTypeEnum.ORACALE:
      return 'ORACALE';
    case DsTypeEnum.PGSQL:
      return 'PGSQL';
    case DsTypeEnum.GOS:
      return 'GOS';
    case DsTypeEnum.REDIS:
      return 'REDIS';
    case DsTypeEnum.HDFS:
      return 'HDFS';
    case DsTypeEnum.HIVE:
      return 'HIVE';
    case DsTypeEnum.MONGODB:
      return 'MONGODB';
    case DsTypeEnum.FTP:
      return 'FTP';
    case DsTypeEnum.PULSAR:
      return 'PULSAR';
    case DsTypeEnum.VIDEOSTREAM:
      return 'VIDEOSTREAM';
    case DsTypeEnum.HBASE:
      return 'HBASE';
    case DsTypeEnum.ES:
      return 'Elasticsearch';

      return type;
  }
};

/**
 * 检测编辑是否需要检测
 * @param ds_type 数据源类型
 * @returns
 */
export const checkInDsTest = (ds_type: number) => {
  return (
    ds_type == DsTypeEnum.MYSQL ||
    ds_type == DsTypeEnum.PGSQL ||
    ds_type == DsTypeEnum.HIVE ||
    ds_type == DsTypeEnum.ES
  );
};

export const PulsarScheamList = ['ByteSchema', 'StringSchema'];

/**
 * 防抖
 * @param func
 * @param wait
 * @returns
 */
export const debounce = (func, wait) => {
  let timeout;
  return function () {
    clearTimeout(timeout);
    timeout = setTimeout(function () {
      func.apply();
    }, wait);
  };
};

import { defHttp } from '/@/utils/http/axios';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  Info = '/api/v1/server_info',
}

/**
 * @description: 查询配置信息
 */
export function getInfo(params: any = null, mode: ErrorMessageMode = 'message') {
  return defHttp.get(
    {
      url: Api.Info,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 添加配置信息
 */
export function addInfo(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.post(
    {
      url: Api.Info,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 更新配置信息
 */
export function updateInfo(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.patch(
    {
      url: Api.Info + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 删除配置信息
 */
export function delInfo(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.delete(
    {
      url: Api.Info + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

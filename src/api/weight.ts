import { defHttp } from '/@/utils/http/axios';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  weight = '/api/v1/bang',
}

/**
 * @description: 查询磅单信息
 */
export function getWeight(params: any = null, mode: ErrorMessageMode = 'message') {
  return defHttp.get(
    {
      url: Api.weight,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 添加磅单信息
 */
export function addWeight(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.post(
    {
      url: Api.weight,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 更新磅单信息
 */
export function updateWeight(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.patch(
    {
      url: Api.weight + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 删除磅单信息
 */
export function delWeight(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.delete(
    {
      url: Api.weight + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

import { defHttp } from '/@/utils/http/axios';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  car = '/api/v1/car_info',
}

/**
 * @description: 查询车辆信息
 */
export function getCars(params: any = null, mode: ErrorMessageMode = 'message') {
  return defHttp.get(
    {
      url: Api.car,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 添加车辆信息
 */
export function addCar(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.post(
    {
      url: Api.car,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 更新车辆信息
 */
export function updateCar(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.patch(
    {
      url: Api.car + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 删除车辆信息
 */
export function delCar(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.delete(
    {
      url: Api.car + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

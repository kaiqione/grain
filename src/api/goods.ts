import { defHttp } from '/@/utils/http/axios';

import { ErrorMessageMode } from '/#/axios';

enum Api {
  good = '/api/v1/grain_info',
}

/**
 * @description: 查询粮食信息
 */
export function getGoods(params: any = null, mode: ErrorMessageMode = 'message') {
  return defHttp.get(
    {
      url: Api.good,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

/**
 * @description: 添加粮食信息
 */
export function addGood(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.post(
    {
      url: Api.good,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 更新粮食信息
 */
export function updateGood(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.patch(
    {
      url: Api.good + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}
/**
 * @description: 删除粮食信息
 */
export function delGood(params: any, mode: ErrorMessageMode = 'message') {
  return defHttp.delete(
    {
      url: Api.good + '/' + params.id,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
}

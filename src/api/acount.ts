import { defHttp } from '/@/utils/http/axios';
import { ErrorMessageMode } from '/#/axios';

enum Api {
  register = '/api/v1/auth/register', // 注册
}

/**
 * @description: 添加用户
 */
export const addUser = (params: any, mode: ErrorMessageMode = 'message') => {
  return defHttp.post(
    {
      url: Api.register,
      params,
    },
    {
      errorMessageMode: mode,
    },
  );
};

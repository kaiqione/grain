import { getCaseList, getGroupList } from '/@/api/acount';

let accountList: any[] = [];
let accountGroupList: any[] = [];

const getAccountList = async () => {
  const { list } = await getCaseList();
  accountList = list;
};

const getAccountGroupList = async () => {
  const { list } = await getGroupList();

  accountGroupList = list;
};

/**
 * 获取用户名
 * @param userId
 * @returns
 */
export const getUserNameById = async (userId: number) => {
  if (accountList.length == 0) await getAccountList();
  const target: any = accountList.find((ele) => {
    return ele.id == userId;
  });

  return target?.name || '未知';
};

export const getUserListByGroup = async (group: number) => {
  if (accountList.length == 0) await getAccountList();
  const tmpList: any[] = [];
  accountList.forEach((ele) => {
    if (ele.groups.indexOf(group) !== -1) {
      tmpList.push({
        label: ele.name,
        value: ele.id,
      });
    }
  });

  return tmpList;
};

/**
 * 获取 分组名
 * @param groupId
 * @returns
 */
export const getUserGroupNameById = async (groupId: number) => {
  if (accountGroupList.length == 0) await getAccountGroupList();

  const target: any = accountGroupList.find((ele) => {
    return ele.id == groupId;
  });

  return target?.name || '未知';
};

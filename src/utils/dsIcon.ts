import MysqlPng from '/@/assets/datasource/mysql.png';
import DM8 from '/@/assets/datasource/dameng.png';
import Oracle from '/@/assets/datasource/oracle.png';
import postgresql from '/@/assets/datasource/postgresql.png';
import redis from '/@/assets/datasource/redis.png';
import hive from '/@/assets/datasource/hive.jpg';
import pulsar from '/@/assets/datasource/pulsar.svg';
import mongodb from '/@/assets/datasource/mongodb.png';
import hbase from '/@/assets/datasource/hbase.png';
import ftp from '/@/assets/datasource/ftp.png';
import starrocks from '/@/assets/datasource/starrocks.png';
import elasticsearch from '/@/assets/datasource/elastic-logo.svg';
import Gos from '/@/assets/datasource/media.svg';
import Ds from '/@/assets/datasource/ds.svg';

// 数据源类型
export enum DsTypeEnum {
  mysql = 'mysql',
  padl2sim = 'padl2sim',
  oracle = 'oracle',
  postgresql = 'postgresql',
  gos = 'gos',
  redis = 'redis',
  hdfs = 'hdfs',
  hive = 'hive',
  mongodb = 'mongodb',
  ftp = 'ftp',
  pulsar = 'pulsar',
  video = 'video',
  fastdfs = 'fastdfs',
  hbase = 'hbase',
  starrocks = 'starrocks',
  elasticsearch = 'elasticsearch',
  finger = 'finger',
  milvus = 'milvus',
  rocket = 'rocket',
  kafka = 'kafka',
  audio = 'audio',
}

/**
 * 获取数据源idon
 */
export const getDsIcon = (ds_type: string) => {
  switch (ds_type) {
    case DsTypeEnum.mysql:
      return MysqlPng;
    case DsTypeEnum.padl2sim:
      return DM8;
    case DsTypeEnum.oracle:
      return Oracle;
    case DsTypeEnum.postgresql:
      return postgresql;
    case DsTypeEnum.gos:
      return Gos;
    case DsTypeEnum.redis:
      return redis;
    case DsTypeEnum.hive:
      return hive;
    case DsTypeEnum.mongodb:
      return mongodb;
    case DsTypeEnum.ftp:
      return ftp;
    case DsTypeEnum.pulsar:
      return pulsar;
    case DsTypeEnum.hbase:
      return hbase;
    case DsTypeEnum.starrocks:
      return starrocks;
    case DsTypeEnum.elasticsearch:
      return elasticsearch;
  }
  return Ds;
};

export const getDsName = (ds_type: string) => {
  switch (ds_type) {
    case DsTypeEnum.mysql:
      return 'MySql';
    case DsTypeEnum.padl2sim:
      return 'DM8';
    case DsTypeEnum.oracle:
      return 'Oracle';
    case DsTypeEnum.postgresql:
      return 'PostgreSQL ';
    case DsTypeEnum.gos:
      return '文件';
    case DsTypeEnum.redis:
      return 'Redis';
    case DsTypeEnum.hive:
      return 'hive';
    case DsTypeEnum.mongodb:
      return 'mongodb';
    case DsTypeEnum.ftp:
      return 'FTP';
    case DsTypeEnum.pulsar:
      return 'Pulsar';
    case DsTypeEnum.hbase:
      return 'hbase';
    case DsTypeEnum.starrocks:
      return 'StarRocks';
    case DsTypeEnum.elasticsearch:
      return 'Elasticsearch';
  }
  return '未知';
};

const hexList: string[] = [];
for (let i = 0; i <= 15; i++) {
  hexList[i] = i.toString(16);
}

export function buildUUID(): string {
  let uuid = '';
  for (let i = 1; i <= 36; i++) {
    if (i === 9 || i === 14 || i === 19 || i === 24) {
      uuid += '-';
    } else if (i === 15) {
      uuid += 4;
    } else if (i === 20) {
      uuid += hexList[(Math.random() * 4) | 8];
    } else {
      uuid += hexList[(Math.random() * 16) | 0];
    }
  }
  return uuid.replace(/-/g, '');
}

let unique = 0;
export function buildShortUUID(prefix = ''): string {
  const time = Date.now();
  const random = Math.floor(Math.random() * 1000000000);
  unique++;
  return prefix + '_' + random + unique + String(time);
}

export const uuidByTime = () => {
  const date = new Date();

  const year = date.getFullYear(); //获取完整的年份(4位)
  const month = date.getMonth() + 1; //获取当前月份(0-11,0代表1月)
  const day = date.getDate(); // 获取当前日(1-31)

  return `${year}${month < 10 ? '0' + month : month}${day < 10 ? '0' + day : day}${Math.random()
    .toFixed(6)
    .toString()
    .slice(2)}`;
};

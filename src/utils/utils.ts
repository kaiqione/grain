import axios from 'axios';
import JSZip from 'jszip';
import FileSaver from 'file-saver';
import html2Canvas from 'html2canvas';
import { domToPng, domToCanvas } from 'modern-screenshot';
import * as XLSX from 'xlsx';
import JsPDF from 'jspdf';

export const getUrlParams = (url: string): any => {
  // str为？之后的参数部分字符串
  const str = url.substring(url.indexOf('?') + 1);
  // arr每个元素都是完整的参数键值
  const arr = str.split('&');
  // result为存储参数键值的集合
  const result = {};
  for (let i = 0; i < arr.length; i++) {
    // item的两个元素分别为参数名和参数值
    const item = arr[i].split('=');
    result[item[0]] = item[1];
  }
  return result;
};

export const fomartDate = (fmt = 'yyyy-MM-dd HH:mm:ss') => {
  const o = {
    'M+': new Date().getMonth() + 1, //月份
    'd+': new Date().getDate(), //日
    'H+': new Date().getHours(), //小时
    'm+': new Date().getMinutes(), //分
    's+': new Date().getSeconds(), //秒
    'q+': Math.floor((new Date().getMonth() + 3) / 3), //季度
    S: new Date().getMilliseconds(), //毫秒
  };
  if (/(y+)/.test(fmt))
    fmt = fmt.replace(RegExp.$1, (new Date().getFullYear() + '').substring(4 - RegExp.$1.length));
  for (const k in o)
    if (new RegExp('(' + k + ')').test(fmt))
      fmt = fmt.replace(
        RegExp.$1,
        RegExp.$1.length == 1 ? o[k] : ('00' + o[k]).substring(('' + o[k]).length),
      );
  return fmt;
};

const getFile = (url) => {
  return new Promise((resolve, reject) => {
    axios({
      method: 'get',
      url,
      responseType: 'arraybuffer',
    })
      .then((data) => {
        resolve(data);
      })
      .catch((error) => {
        reject(error.toString());
      });
  });
};

const ContentTypeFormat = [
  {
    contentType: 'application/vnd.ms-powerpoint',
    extension: '.ppt',
  },
  {
    contentType: 'application/pdf',
    extension: '.pdf',
  },
  {
    contentType: 'application/xml',
    extension: '.xml',
  },
  {
    contentType: 'text/html',
    extension: '.html',
  },
  {
    contentType: 'text/xml',
    extension: '.xml',
  },
  {
    contentType: 'image/gif',
    extension: '.gif',
  },
  {
    contentType: 'image/jpeg',
    extension: '.jpg',
  },
  {
    contentType: 'image/png',
    extension: '.png',
  },
  { contentType: 'application/msword', extension: '.doc' },
  { contentType: 'application/vnd.ms-excel', extension: '.xls' },
  {
    contentType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
    extension: '.docx',
  },
  {
    contentType: 'application/vnd.openxmlformats-officedocument.wordprocessingml.template',
    extension: '.docx',
  },

  {
    contentType: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    extension: '.xlsx',
  },
];

export const downMultToZip = async (urls: any[], zipName = '批量打包') => {
  const zip = new JSZip();
  // const cache = {};
  const promises: any[] = [];
  urls.forEach((item) => {
    const promise = getFile(item.url).then((file: any) => {
      const targetFormat: any = ContentTypeFormat.find((ele) => {
        return ele.contentType == file.headers['content-type'];
      });
      const extension: string = targetFormat?.extension || '';
      // 下载文件, 并存成ArrayBuffer对象
      zip.file(item.name + extension, file.data, { binary: true }); // 逐个添加文件
      // cache[file_name] = data;
    });
    promises.push(promise);
  });

  await Promise.all(promises).then(() => {
    zip.generateAsync({ type: 'blob' }).then((content) => {
      // 生成二进制流
      FileSaver.saveAs(content, `${zipName}.zip`); // 利用file-saver保存文件
    });
  });
};

/**
 * html导出图片
 */
export const ExportPng = async (id: string, fileName = '文件') => {
  const element = document.getElementById(id)!;
  await domToPng(element, {
    backgroundColor: '#f5f7fa',
    scale: 5,
  })
    .then((dataUrl) => {
      const link = document.createElement('a');
      link.download = `${fileName}.png`;
      link.href = dataUrl;
      link.click();
    })
    .catch((error) => {
      console.log('导出失败', error);
    })
    .finally(() => {});
};

const getCellWidth = (value) => {
  // 判断是否为null或undefined
  if (value == null) {
    return 10;
  } else if (/.*[\u4e00-\u9fa5]+.*$/.test(value)) {
    // 中文的长度
    const chineseLength = value.match(/[\u4e00-\u9fa5]/g).length;
    // 其他不是中文的长度
    const otherLength = value.length - chineseLength;
    return chineseLength * 2.1 + otherLength * 1.1;
  } else {
    return value.toString().length * 1.1;
    /* 另一种方案
    value = value.toString()
    return value.replace(/[\u0391-\uFFE5]/g, 'aa').length
    */
  }
};

/**
 * 导出excel
 * @param tableData 数据 [{"name": "张三", "age": 18}]
 * @param fields 字段信息 {"name": "名字", "age": "年龄" }
 * @param fileName 文件名
 */
export const ExportExcel = (tableData: any[], fields: any, fileName: string) => {
  // 获取标准格式数据
  const tmpList: any[] = tableData.map((ele) => {
    const obj: any = {};
    for (const k in fields) {
      obj[fields[k]] = ele[k];
    }
    return obj;
  });
  // 创建工作表
  const worksheet: any = XLSX.utils.json_to_sheet(tmpList);
  // 计算列最大宽度
  const colWidths: any[] = [];
  const colNames = Object.keys(tmpList[0]); // 所有列的名称数组
  colNames.forEach((key: any, index: number) => {
    if (colWidths[index] == null) colWidths[index] = [getCellWidth(key)];
    tmpList.forEach((ele) => {
      switch (typeof ele[key]) {
        case 'string':
        case 'number':
        case 'boolean':
          colWidths[index].push(getCellWidth(ele[key]));
          break;
        case 'object':
        case 'function':
          colWidths[index].push(0);
          break;
      }
    });
  });
  worksheet['!cols'] = [];
  colWidths.forEach((widths: any) => {
    worksheet['!cols'].push({ wch: Math.max(...widths) + 2 });
  });
  // 创建工作薄
  const workbook = XLSX.utils.book_new();
  // 将工作表放入工作薄中
  XLSX.utils.book_append_sheet(workbook, worksheet, fileName);
  // 生成文件并下载
  XLSX.writeFile(workbook, `${fileName}.xlsx`);
};

/**
 * 生成自定义大小的图片
 * @param id
 * @param format
 * @param orientation
 */
export const resizedataURL = (id: string) => {
  const element = document.getElementById(id)!;
  return new Promise((resolve, reject) => {
    domToCanvas(element, {
      backgroundColor: '#fff',
      scale: 5,
    })
      .then(async (canvas) => {
        const pageData = canvas.toDataURL('image/jpeg', 1.0);
        // const img = new Image();
        // img.src = pageData;
        // img.onload = () => {
        //   //创建一个canvas
        //   const canvas = document.createElement('canvas');
        //   //设置大小（伸展或缩小尺寸）
        //   canvas.width = ((orientation == 'l' ? targetSize[1] : targetSize[0]) * 300) / 25.4;
        //   canvas.height = (canvas.width / img.width) * img.height;
        //   //设置画笔
        //   const ctx = canvas.getContext('2d')!;
        //   ctx.drawImage(img, 0, 0, img.width, img.height, 0, 0, canvas.width, canvas.height);
        //   //把改变尺寸的图片设置为一个base64
        //   const newUrl = canvas.toDataURL();
        //   resolve(newUrl);
        // };
        resolve(pageData);
      })
      .catch((error) => {
        console.log('生成自定义大小的图片失败', error);
        reject(error);
      })
      .finally(() => {});
  });
};

/**
 * html导出pdf
 * @param id
 * @param fileName
 * @param format
 * @param orientation  纵向:p  横向:l
 * @returns
 */
export const ExportPdf = async (
  id: string,
  fileName = '文件',
  format: 'a4' | 'a5' | 'a6' = 'a5',
  orientation: 'p' | 'portrait' | 'l' | 'landscape' = 'l',
) => {
  const element = document.getElementById(id)!;

  const sizeObj = {
    a4: [210, 297],
    a5: [148, 210],
    a6: [105, 148],
  };
  const targetSize: number[] = sizeObj[format];

  await domToCanvas(element, {
    backgroundColor: '#fff',
    scale: 5,
  })
    .then((canvas) => {
      // 画布宽高
      const contentWidth = canvas.width;
      const contentHeight = canvas.height;
      // 一页pdf显示html页面生成的canvas高度;
      const pageHeight =
        orientation == 'l'
          ? (contentWidth / targetSize[1]) * targetSize[0]
          : (contentWidth / targetSize[0]) * targetSize[1];
      // 未生成pdf的html页面高度
      let leftHeight = contentHeight;
      // 页面偏移
      let position = 0;
      // a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
      const imgWidth = orientation == 'l' ? targetSize[1] : targetSize[0];
      const imgHeight =
        ((orientation == 'l' ? targetSize[1] : targetSize[0]) / contentWidth) * contentHeight;
      const pageData = canvas.toDataURL('image/jpeg', 1.0);
      // a4纸纵向，一般默认使用；new JsPDF('landscape');
      const PDF = new JsPDF(orientation, 'mm', format, false);

      // 当内容未超过pdf一页显示的范围，无需分页
      if (leftHeight < pageHeight) {
        // addImage(pageData, 'JPEG', 左，上，宽度，高度)设置
        PDF.addImage(pageData, 'JPEG', 0, 0, imgWidth, imgHeight);
      } else {
        // 超过一页时，分页打印（每页高度841.89）
        while (leftHeight > 0) {
          PDF.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight);
          leftHeight -= pageHeight;
          position -= orientation == 'l' ? targetSize[0] : targetSize[1];
          if (leftHeight > 0) {
            PDF.addPage();
          }
        }
      }
      PDF.save(`${fileName}.pdf`);
    })
    .catch((error) => {
      console.log('导出失败', error);
    })
    .finally(() => {});

  return;
  const opts = {
    scale: 10, // 缩放比例，提高生成图片清晰度
    useCORS: true, // 允许加载跨域的图片
    allowTaint: true, // 允许图片跨域，和 useCORS 二者不可共同使用
    tainttest: true, // 检测每张图片已经加载完成
    logging: false, // 日志开关，发布的时候记得改成 false
  };

  html2Canvas(element, opts)
    .then((canvas) => {
      const contentWidth = canvas.width;
      const contentHeight = canvas.height;
      // 一页pdf显示html页面生成的canvas高度;
      const pageHeight = (contentWidth / 592.28) * 841.89;
      // 未生成pdf的html页面高度
      let leftHeight = contentHeight;
      // 页面偏移
      let position = 0;
      // a4纸的尺寸[595.28,841.89]，html页面生成的canvas在pdf中图片的宽高
      const imgWidth = 595.28;
      const imgHeight = (592.28 / contentWidth) * contentHeight;
      const pageData = canvas.toDataURL('image/jpeg', 1.0);
      // a4纸纵向，一般默认使用；new JsPDF('landscape'); 横向页面
      const PDF = new JsPDF('p', 'pt', 'a4');

      // 当内容未超过pdf一页显示的范围，无需分页
      if (leftHeight < pageHeight) {
        // addImage(pageData, 'JPEG', 左，上，宽度，高度)设置
        PDF.addImage(pageData, 'JPEG', 0, 0, imgWidth, imgHeight);
      } else {
        // 超过一页时，分页打印（每页高度841.89）
        while (leftHeight > 0) {
          PDF.addImage(pageData, 'JPEG', 0, position, imgWidth, imgHeight);
          leftHeight -= pageHeight;
          position -= 841.89;
          if (leftHeight > 0) {
            PDF.addPage();
          }
        }
      }
      PDF.save(`${fileName}.pdf`);
    })
    .catch((error) => {
      console.log('导出失败', error);
    })
    .finally(() => {});
};

/**
 * 获取excel数据
 * @param file
 * @param callback
 */
export const getExcelData = (file, callback, limit = -1) => {
  const reader = new FileReader();

  reader.onload = (e) => {
    const data = e.target?.result;
    const contents: any = [];

    const workbook = XLSX.read(data, { type: 'binary' });
    for (const sheet in workbook.Sheets) {
      if (workbook.Sheets.hasOwnProperty(sheet)) {
        contents.push(XLSX.utils.sheet_to_json(workbook.Sheets[sheet]));
      }
    }
    if (callback) callback(limit <= 0 ? contents : contents.slice(0, limit));
  };
  reader.readAsBinaryString(file);
  // reader.readAsText(file, 'UTF-8');
};

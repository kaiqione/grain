import { getClassificationData } from '/@/api/laws';

/**
 * 标签
 */
export enum Tags {
  BIAO_ZHUN = 1, // 标准类型
  GUO_BIAO = 2,
  HANG_BIAO = 3,
  REGION_ID = 4,
  DI_YU = 382, // 地域
  ZHENG_CE_FA_GUI = 378, //政策法规
  FA_BU_DAN_WEI = 203,
  FA_BU_RI_QI = 362,
  SHENG_XIAO_RI_QI = 370,
  DATA_EXPORT_TASK_STATUS = 412, // 数据出境任务状态
  DATA_SIZE_UNITS = 419, // 数据规模
  POLICY_AND_STANDARD_STATUS = 423,
  INDUSTRY_ID = 426, // 行业领域
  SHEN_BAO_STANDARD_ID = 427, // 申报标准
  ASSESS_TASK_STATUS = 440, // 数据安全评估任务状态
  SHEN_PI_HUAN_JIE = 441, // 审批环节
  SHEN_PI_QUAN_XIAN = 445, // 审批权限
  ZHU_GUAN_BU_MEN = 464,
  DATA_EXPORT_RECORD_STATUS = 559,
}

export interface OptionInter {
  id: number | string;
  label: string;
  value: string | number;
  // showTime?: boolean;
  multiple?: boolean;
  // showSearch?: boolean;
  showText?: boolean;
  // showTab?: boolean;
  type: number; // 0：普通 1：tab 2:search 3: time
}

/**
 * 获取数据
 * @param id
 */
export const getTags = async (id: number | string, level = 1): Promise<OptionInter[]> => {
  const data = await getClassificationData({ id, level });
  const options: OptionInter[] = data[0].children.map((ele) => {
    return {
      id: ele.id,
      label: ele.label,
      value: ele.value,
      children: ele.children || [],
    };
  });
  return options;
};

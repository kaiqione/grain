export const DeepCopy = (data: Object): Object | null => {
  if (data) {
    return JSON.parse(JSON.stringify(data));
  }

  return null;
};

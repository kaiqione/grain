import '/@/design/index.less';
import 'virtual:windi-base.css';
import 'virtual:windi-components.css';
import 'virtual:windi-utilities.css';
// Register icon sprite
import 'virtual:svg-icons-register';
import App from './App.vue';
import { createApp } from 'vue';
import { initAppConfigStore } from '/@/logics/initAppConfig';
import { setupErrorHandle } from '/@/logics/error-handle';
import { router, setupRouter } from '/@/router';
import { setupRouterGuard } from '/@/router/guard';
import { setupStore } from '/@/store';
import { setupGlobDirectives } from '/@/directives';
import { setupI18n } from '/@/locales/setupI18n';
import { registerGlobComp } from '/@/components/registerGlobComp';
// import axios from 'axios';
import EventBus from '/@/utils/lib/eventBus';
// import { addAPIProvider, disableCache } from '@iconify/iconify';
import { PROJ_CFG_KEY } from '/@/enums/cacheEnum';
import { Persistent } from '/@/utils/cache/persistent';
import print from 'vue3-print-nb';

const $bus = new EventBus();

// addAPIProvider('', {
//   resources: ['/localIconify'],
// });

// disableCache('all');

// Importing on demand in local development will increase the number of browser requests by around 20%.
// This may slow down the browser refresh speed.
// Therefore, only enable on-demand importing in production environments .
if (import.meta.env.DEV) {
  import('ant-design-vue/dist/antd.less');
}
async function bootstrap() {
  const app = createApp(App);

  // 清理缓存
  // localStorage.clear();
  // 清除项目配置缓存
  Persistent.removeLocal(PROJ_CFG_KEY, true);

  // Configure store
  setupStore(app);

  // Initialize internal system configuration
  initAppConfigStore();

  // Register global components
  registerGlobComp(app);

  // Multilingual configuration
  // Asynchronous case: language files may be obtained from the server side
  await setupI18n(app);

  // Configure routing
  setupRouter(app);

  // router-guard
  setupRouterGuard(router);

  // Register global directive
  setupGlobDirectives(app);

  // Configure global error handling
  setupErrorHandle(app);

  // https://next.router.vuejs.org/api/#isready
  // await router.isReady();

  await initConfig(app);

  app.provide('$bus', $bus);

  app.use(print);

  app.mount('#app');
}

bootstrap();

const initConfig = async (app: any) => {
  // await axios.get('/config.json').then((res: any) => {
  //   app.provide('onlyofficeurl', res.data.ONLYOFFICE_SERVER_URL);
  // });
  const systemUrl = `${window.location.protocol}//${window.location.host}${
    import.meta.env.VITE_GLOB_API_URL
  }`;
  app.provide('systemUrl', systemUrl);
};

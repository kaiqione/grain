import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';
// import { PermissionsEnum } from '/@/enums/permissionsEnum';

const pound: AppRouteModule = {
  path: '/pound',
  name: 'pound',
  component: LAYOUT,
  redirect: '/pound/index',
  meta: {
    // hideMenu: true,
    hideChildrenInMenu: true,
    icon: 'la:weight',
    title: t('routes.grain.pound'),
    orderNo: 3,
    // roles: [PermissionsEnum.PANEL_MENU],
  },
  children: [
    {
      path: 'index',
      name: 'poundIndex',
      component: () => import('/@/views/pound/index.vue'),
      meta: {
        title: t('routes.grain.pound'),
        hideMenu: true,
        // roles: [PermissionsEnum.PANEL_MENU],
        hideBreadcrumb: true,
      },
    },
  ],
};

export default pound;

import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';
// import { PermissionsEnum } from '/@/enums/permissionsEnum';

const car: AppRouteModule = {
  path: '/car',
  name: 'car',
  component: LAYOUT,
  redirect: '/car/index',
  meta: {
    // hideMenu: true,
    hideChildrenInMenu: true,
    icon: 'ph:truck',
    title: t('routes.grain.car'),
    orderNo: 5,
    // roles: [PermissionsEnum.PANEL_MENU],
  },
  children: [
    {
      path: 'index',
      name: 'carIndex',
      component: () => import('/@/views/car/index.vue'),
      meta: {
        title: t('routes.grain.car'),
        hideMenu: true,
        // roles: [PermissionsEnum.PANEL_MENU],
        hideBreadcrumb: true,
      },
    },
  ],
};

export default car;

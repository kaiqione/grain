import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';
// import { PermissionsEnum } from '/@/enums/permissionsEnum';

const goods: AppRouteModule = {
  path: '/goods',
  name: 'goods',
  component: LAYOUT,
  redirect: '/goods/index',
  meta: {
    // hideMenu: true,
    hideChildrenInMenu: true,
    icon: 'ph:grains-duotone',
    title: t('routes.grain.goods'),
    orderNo: 6,
    // roles: [PermissionsEnum.PANEL_MENU],
  },
  children: [
    {
      path: 'index',
      name: 'goodsIndex',
      component: () => import('/@/views/goods/index.vue'),
      meta: {
        title: t('routes.grain.goods'),
        hideMenu: true,
        // roles: [PermissionsEnum.PANEL_MENU],
        hideBreadcrumb: true,
      },
    },
  ],
};

export default goods;

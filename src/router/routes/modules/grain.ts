import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';
// import { PermissionsEnum } from '/@/enums/permissionsEnum';

const grain: AppRouteModule = {
  path: '/grain',
  name: 'grain',
  component: LAYOUT,
  redirect: '/grain/index',
  meta: {
    hideChildrenInMenu: true,
    icon: 'solar:card-transfer-linear',
    title: t('routes.grain.mgr'),
    orderNo: 1,
    // roles: [PermissionsEnum.PANEL_MENU],
  },
  children: [
    {
      path: 'index',
      name: 'grainIndex',
      component: () => import('/@/views/grain/index.vue'),
      meta: {
        title: t('routes.grain.mgr'),
        hideMenu: true,
        // roles: [PermissionsEnum.PANEL_MENU],
        hideBreadcrumb: true,
      },
    },
  ],
};

export default grain;

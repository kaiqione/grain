import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';

const system: AppRouteModule = {
  path: '/system',
  name: 'system',
  component: LAYOUT,
  redirect: '/system/pound',
  meta: {
    icon: 'tabler:settings',
    title: t('routes.system.system'),
    // icon: 'material-symbols:supervisor-account-outline',
    // title: t('routes.system.case'),
    orderNo: 99,
    hideMenu: true,
    // hideChildrenInMenu: true,
  },
  children: [
    {
      path: 'index',
      name: 'ststemIndex',
      component: () => import('/@/views/system/index.vue'),
      meta: {
        title: t('routes.system.case'),
        icon: 'material-symbols:supervisor-account-outline',
        hideMenu: true,
        hideBreadcrumb: true,
      },
    },
    {
      path: 'pound',
      name: 'poundIndex1',
      component: () => import('/@/views/pound/index.vue'),
      meta: {
        title: t('routes.grain.pound'),
        icon: 'la:weight',
      },
    },
    {
      path: 'person',
      name: 'personIndex1',
      component: () => import('/@/views/person/index.vue'),
      meta: {
        title: t('routes.grain.person'),
        icon: 'fluent:person-tag-24-regular',
      },
    },
    {
      path: 'car',
      name: 'carIndex1',
      component: () => import('/@/views/car/index.vue'),
      meta: {
        title: t('routes.grain.car'),
        icon: 'ph:truck',
      },
    },
    {
      path: 'goods',
      name: 'goodsIndex1',
      component: () => import('/@/views/goods/index.vue'),
      meta: {
        title: t('routes.grain.goods'),
        icon: 'ph:grains-duotone',
      },
    },
    {
      path: 'info',
      name: 'infoIndex1',
      component: () => import('/@/views/info/index.vue'),
      meta: {
        title: t('routes.grain.info'),
        icon: 'carbon:operations-field',
      },
    },
  ],
};

export default system;

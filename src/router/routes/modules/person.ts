import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';
// import { PermissionsEnum } from '/@/enums/permissionsEnum';

const person: AppRouteModule = {
  path: '/person',
  name: 'person',
  component: LAYOUT,
  redirect: '/person/index',
  meta: {
    // hideMenu: true,
    hideChildrenInMenu: true,
    icon: 'fluent:person-tag-24-regular',
    title: t('routes.grain.person'),
    orderNo: 4,
    // roles: [PermissionsEnum.PANEL_MENU],
  },
  children: [
    {
      path: 'index',
      name: 'personIndex',
      component: () => import('/@/views/person/index.vue'),
      meta: {
        title: t('routes.grain.person'),
        hideMenu: true,
        // roles: [PermissionsEnum.PANEL_MENU],
        hideBreadcrumb: true,
      },
    },
  ],
};

export default person;

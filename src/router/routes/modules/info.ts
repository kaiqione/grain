import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';
// import { PermissionsEnum } from '/@/enums/permissionsEnum';

const info: AppRouteModule = {
  path: '/info',
  name: 'info',
  component: LAYOUT,
  redirect: '/info/index',
  meta: {
    // hideMenu: true,
    hideChildrenInMenu: true,
    icon: 'carbon:operations-field',
    title: t('routes.grain.info'),
    orderNo: 7,
    // roles: [PermissionsEnum.PANEL_MENU],
  },
  children: [
    {
      path: 'index',
      name: 'infoIndex',
      component: () => import('/@/views/info/index.vue'),
      meta: {
        title: t('routes.grain.info'),
        hideMenu: true,
        // roles: [PermissionsEnum.PANEL_MENU],
        hideBreadcrumb: true,
      },
    },
  ],
};

export default info;

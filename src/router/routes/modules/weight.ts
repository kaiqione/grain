import type { AppRouteModule } from '/@/router/types';

import { LAYOUT } from '/@/router/constant';
import { t } from '/@/hooks/web/useI18n';
// import { PermissionsEnum } from '/@/enums/permissionsEnum';

const weight: AppRouteModule = {
  path: '/weight',
  name: 'weight',
  component: LAYOUT,
  redirect: '/weight/index',
  meta: {
    hideChildrenInMenu: true,
    icon: 'clarity:details-line',
    title: t('routes.grain.weight'),
    orderNo: 2,
    // roles: [PermissionsEnum.PANEL_MENU],
  },
  children: [
    {
      path: 'index',
      name: 'weightIndex1',
      component: () => import('/@/views/weight/index.vue'),
      meta: {
        title: t('routes.grain.weight'),
        hideMenu: true,
        // roles: [PermissionsEnum.PANEL_MENU],
        hideBreadcrumb: true,
      },
    },
  ],
};

export default weight;

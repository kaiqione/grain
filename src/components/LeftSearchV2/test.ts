export const testData = [
  {
    name: '全部',
    key: 'ALL',
    operateLevel: 0,
    children: [],
  },
  {
    name: '默认分组',
    key: '1',
    operateLevel: 0,
    children: [],
  },
  {
    name: '人员信息组',
    key: '2',
    operateLevel: 2,
    children: [
      {
        name: 'AAAA',
        key: '3',
        operateLevel: 2,
        children: [
          {
            name: 'AABB',
            key: '7',
            operateLevel: 2,
            children: [],
          },
          {
            name: 'BAAB',
            key: '8',
            operateLevel: 2,
            children: [],
          },
        ],
      },
      {
        name: 'BBBB',
        key: '6',
        operateLevel: 2,
        children: [],
      },
    ],
  },
  {
    name: '网络信息组',
    key: '4',
    operateLevel: 2,
    children: [
      {
        name: 'BBBB',
        key: '5',
        operateLevel: 2,
        children: [],
      },
    ],
  },
];

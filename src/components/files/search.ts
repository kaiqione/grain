import { getSourceAdapterList, getAllData } from '/@/api/dataAssets';

/**
 * 操作权限
 */
export enum OperationLevelEnum {
  NULL = 0,
  DEL_EDIT = 1,
  ALL = 2,
}

export interface ItemSchema {
  name: string;
  key: string;
  operateLevel: number; // 0：不许任何操作 1：仅删除、编辑 2：最高权限，可删除、编辑、添加子节点
  children: any[];
  edit?: boolean;
  new?: boolean;

  id: number;
  type: string;
  tag: string;
  icon?: string;
  hidden: number;
  disabled: number;
  comment: string;
  ds_type: number;
  pid?: number;
  popover?: boolean;
  popover2?: boolean;
}

export const GetDsType = async (needSecondLevel: boolean) => {
  const { adapters } = await getSourceAdapterList();
  let dsTypeList: ItemSchema[] = []; // 所有的数据类型
  adapters.forEach((ele: any) => {
    if (ele.list.length > 0) {
      dsTypeList = dsTypeList.concat(ele.list);
    }
  });
  // 按类型名去重;
  const tmpList: any[] = [];
  dsTypeList.forEach((ele) => {
    const existIndex = tmpList.findIndex((ele1) => {
      return ele1.type == ele.type;
    });
    if (existIndex == -1) {
      tmpList.push(ele);
    }
  });
  dsTypeList = tmpList;
  // 最终数据
  const finalList: ItemSchema[] = [];
  if (needSecondLevel) {
    const dsList: any[] = await getDs();
    dsList.forEach((ds: any) => {
      const typeId = ds.type;
      const target: ItemSchema = dsTypeList.find((ele) => {
        return ele?.type == typeId;
      })!;
      if (target) {
        target.name = target.type;
        // target.key = '' + target.id;
        target.key = target.type;
        if (!target.children) {
          target.children = [];
        }
        target.children?.push({ ...ds, key: '' + ds.id, operateLevel: 0, children: [] });
      }
    });

    // 去掉没有数据的类型
    dsTypeList.forEach((ele: ItemSchema) => {
      if ((ele.children?.length || 0) > 0) {
        finalList.push(ele);
      }
    });
  } else {
    dsTypeList.forEach((ele: ItemSchema) => {
      finalList.push({
        ...ele,
        name: ele.type,
        // key: '' + ele.id,
        key: ele.type,
        operateLevel: 0,
        children: [],
      });
    });
  }
  return [...finalList];
};
/**
 * 获取所有数据源
 */
const getDs = async () => {
  const { datasource } = await getAllData();

  return datasource;
};

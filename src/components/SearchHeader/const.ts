interface OptionInter {
  label: string;
  value: string | number;
}

export interface ItemInter {
  name: string;
  type: string; // select input datatime datepicker
  placeholder: string;
  options?: OptionInter[];
}

export const Items = [
  {
    name: 'input',
    type: 'input',
    placeholder: '123123',
  },
  {
    name: 'select',
    type: 'select',
    placeholder: '123123',
    options: [
      {
        label: 'haha',
        value: 1,
      },
      {
        label: 'lala',
        value: 2,
      },
    ],
  },
  {
    name: 'datepicker',
    type: 'datepicker',
    placeholder: '123123',
  },
];

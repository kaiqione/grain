export const testData = [
  {
    name: '全部',
    key: '0',
    default: true,
    children: [
      {
        name: 'AAAA',
        key: '0',
      },
      {
        name: 'BBBB',
        key: '1',
      },
    ],
  },
  {
    name: '默认分组',
    key: '1',
    default: true,
    children: [
      {
        name: 'BBBB',
        key: '1',
      },
    ],
  },
  {
    name: '人员信息组',
    key: '2',
    children: [
      {
        name: 'AAAA',
        key: '0',
      },
    ],
  },
];

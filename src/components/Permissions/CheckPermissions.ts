import { useUserStore } from '/@/store/modules/user';
const userStore = useUserStore();

/**
 * 检测权限
 */
export const CheckPermission = (permissionKey: string | null): boolean => {
  if (!permissionKey) {
    return false;
  }
  // 所有权限
  const permissions: any[] = userStore.getRoleList;
  const targetKey: string = permissions.find((ele) => {
    return ele === permissionKey;
  });
  return targetKey ? true : false;
};

/**
 * 检测多个权限
 * @param permissionKeys
 * @param all 是否全部有权限
 * @returns
 */
export const CheckPermissionMutil = (permissionKeys: string[], all = false): boolean => {
  if (permissionKeys.length == 0) {
    return false;
  }
  // 所有权限
  const permissions: any[] = userStore.getRoleList;
  const targetKey: string = permissions.find((ele) => {
    return (
      (!all && permissionKeys.indexOf(ele) !== -1) || (all && permissionKeys.indexOf(ele) === -1)
    );
  });
  return (!all && targetKey) || (all && !targetKey) ? true : false;
};

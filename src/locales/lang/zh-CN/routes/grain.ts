export default {
  mgr: '出入库管理',
  person: '人员信息',
  car: '车辆信息',
  goods: '物品信息',
  pound: '过磅信息',
  info: '配置管理',
  weight: '磅单管理',
};

export default {
  system: '系统设置',
  case: '用户管理',
  permission: '权限管理',
  role: '角色管理',
  roleGroup: '用户组管理',
  user: '用户管理',
};
